/*
 * Constants
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

public interface Constants
{
	// Marvel
	public String KEY_PUBLIC = "776cbb956f54e7161ab99f74a980fc93";
	public String KEY_PRIVATE = "9d2b73afb95c9dac8f38c7c3d73b97cf0fe1646a";

	public String URL_BASE = "http://gateway.marvel.com:80";
	public String URL_FETCH_COMICS = "/v1/public/comics";
	public String URL_PARM_API = "apikey=" + KEY_PUBLIC;
	public String URL_PARM_HASH = "hash=";
	public String URL_PARM_TS = "ts=";
	public String URL_PARM_DELIM_FIRST = "?";
	public String URL_PARM_DELIM_SUBSEQ = "&";

	// JSON and INTENT Key Exchanges
	public String XCHG_KEY_DATA = "data";
	public String XCHG_KEY_RESULTS = "results";
	public String XCHG_KEY_TITLE = "title";
	public String XCHG_KEY_DESC = "description";
	public String XCHG_KEY_FORMAT = "format";
	public String XCHG_KEY_PAGECOUNT = "pageCount";
	public String XCHG_KEY_ISBN = "isbn";
	public String XCHG_KEY_THUMBNAIL = "thumbnail";
	public String XCHG_KEY_THUMBNAIL_PATH = "path";
	public String XCHG_KEY_THUMBNAIL_EXT = "extension";

	// IDs
	public static int ID_VIEW_LIST = 0;
	public static int ID_VIEW_GRID = 1;

	// Shared Preferences
	public static final String PREFERENCES_FILE_NAME = "aecomics_settings";
	public static final String PREF_SHOW_IMAGES = "SHOW_IMAGES";
	public static boolean DEF_SHOW_IMAGES = true;
}