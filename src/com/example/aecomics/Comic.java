/*
 * Comic
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

public class Comic
{	
	private String mstrTitle;
	private String mstrDescription;
	private String mstrFormat;
	private String mstrPageCount;
	private String mstrISBN;
	private String mstrThumbnailPath;
	private String mstrThumbnailExtension;

	Comic()
	{	
		mstrTitle = "";
		mstrDescription = "";
		mstrFormat = "";
		mstrPageCount = "";
		mstrISBN = "";				
	}
	
	public String getTitle()
	{
		return mstrTitle;
	}

	public void setTitle(String strTitle)
	{
		this.mstrTitle = strTitle;
	}

	public String getDescription()
	{
		return mstrDescription;
	}

	public void setDescription(String mstrDescription)
	{
		this.mstrDescription = mstrDescription;
	}

	public String getFormat()
	{
		return mstrFormat;
	}

	public void setFormat(String strFormat)
	{
		this.mstrFormat = strFormat;
	}

	public String getISBN()
	{
		return mstrISBN;
	}

	public void setISBN(String strISBN)
	{
		this.mstrISBN = strISBN;
	}

	public String getPageCount()
	{
		return mstrPageCount;
	}

	public void setPageCount(String strPageCount)
	{
		this.mstrPageCount = strPageCount;
	}

	public String getThumbnailPath()
	{
		return mstrThumbnailPath;
	}

	public void setThumbnailPath(String strThumbnailPath)
	{
		this.mstrThumbnailPath = strThumbnailPath;
	}

	public String getThumbnailExtension()
	{
		return mstrThumbnailExtension;
	}

	public void setThumbnailExtension(String strThumbnailExtension)
	{
		this.mstrThumbnailExtension = strThumbnailExtension;
	}
}