/*
 * ActivityMain
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;

public class ActivityMain extends Activity
{
	// Primitives
	private boolean mbGridView;
	private ArrayList<Comic> malComics;

	// Android
	private SharedPreferences mPreferences;

	// UI
	private ListView mlvComics;
	private GridView mgvComics;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayShowHomeEnabled(false);

		ColorDrawable colorDrawable = new ColorDrawable();
		colorDrawable.setColor(ContextCompat.getColor(getBaseContext(), R.color.actionbar_bg));
		currentActionBar.setBackgroundDrawable(colorDrawable);

		mbGridView = false;
		malComics = new ArrayList<Comic>();
		mPreferences = getSharedPreferences(Constants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		
		mlvComics = (ListView) findViewById(R.id.lv_comics);
		mgvComics = (GridView) findViewById(R.id.gv_comics);

		RequestQueue queue = Volley.newRequestQueue(this);
		String strTimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.CANADA).format(new java.util.Date());

		MessageDigest md5;
		String strHash = "";
		try
		{
			md5 = MessageDigest.getInstance("MD5");
			md5.update(StandardCharsets.UTF_8.encode(strTimeStamp + Constants.KEY_PRIVATE + Constants.KEY_PUBLIC));
			strHash = String.format("%032x", new BigInteger(1, md5.digest()));
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}

		String url = Constants.URL_BASE + Constants.URL_FETCH_COMICS + Constants.URL_PARM_DELIM_FIRST
				+ Constants.URL_PARM_API + Constants.URL_PARM_DELIM_SUBSEQ + Constants.URL_PARM_HASH + strHash
				+ Constants.URL_PARM_DELIM_SUBSEQ + Constants.URL_PARM_TS + strTimeStamp;

		StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>()
		{
			@Override
			public void onResponse(String response)
			{
				decodeComics(response);
				((AdapterComics) mlvComics.getAdapter()).notifyDataSetChanged();
			}
		}, new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error)
			{
			}
		});
		queue.add(stringRequest);
		
		AdapterComics adapterComicsList = new AdapterComics(ActivityMain.this, R.layout.listitem_comic, malComics, this,
				mPreferences, false);
		mlvComics.setAdapter(adapterComicsList);

		AdapterComics adapterComicsGrid = new AdapterComics(ActivityMain.this, R.layout.listitem_comic, malComics, this,
				mPreferences, true);
		mgvComics.setAdapter(adapterComicsGrid);

		showAppropriateView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.main, menu);

		MenuItem item = (MenuItem) menu.findItem(R.id.action_changeview);
		if (mbGridView)
		{
			item.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.icon_list, null));
		}
		else
		{
			item.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.icon_grid, null));
		}
		
		boolean bShowImages = mPreferences.getBoolean(Constants.PREF_SHOW_IMAGES, Constants.DEF_SHOW_IMAGES);
		item.setVisible(bShowImages);
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		switch (id)
		{
		case R.id.action_changeview:
			mbGridView = !mbGridView;
			showAppropriateView();
			if (mbGridView)
			{
				item.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.icon_list, null));
			}
			else
			{
				item.setIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.icon_grid, null));
			}
			break;
		case R.id.action_settings:
			DialogSettings dialogSettings = new DialogSettings(ActivityMain.this, mPreferences);
			dialogSettings.displayDialog();
			dialogSettings.setOnCancelListener(new OnCancelListener()
			{
				@Override
				public void onCancel(DialogInterface dialog)
				{
					Intent intentHome = new Intent(ActivityMain.this, ActivityMain.class);
					intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentHome);
					finish();
				}
			});
			dialogSettings.setOnDismissListener(new OnDismissListener()
			{
				@Override
				public void onDismiss(DialogInterface dialog)
				{
					Intent intentHome = new Intent(ActivityMain.this, ActivityMain.class);
					intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentHome);
					finish();
				}
			});
			break;
		case R.id.action_about:
			DialogAbout dialogAbout = new DialogAbout(ActivityMain.this);
			dialogAbout.displayDialog();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void decodeComics(String strJSON)
	{
		try
		{
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(strJSON);

			JSONObject jobjData = (JSONObject) jsonObject.get(Constants.XCHG_KEY_DATA);

			JSONArray jarrResults = (JSONArray) jobjData.get(Constants.XCHG_KEY_RESULTS);
			@SuppressWarnings("rawtypes")
			Iterator itrResults = jarrResults.iterator();

			while (itrResults.hasNext())
			{
				JSONObject jobjResult = (JSONObject) itrResults.next();

				Comic comic = new Comic();

				if (jobjResult.get(Constants.XCHG_KEY_TITLE) != null)
					comic.setTitle(jobjResult.get(Constants.XCHG_KEY_TITLE).toString());
				if (jobjResult.get(Constants.XCHG_KEY_DESC) != null)
					comic.setDescription(jobjResult.get(Constants.XCHG_KEY_DESC).toString());
				if (jobjResult.get(Constants.XCHG_KEY_FORMAT) != null)
					comic.setFormat(jobjResult.get(Constants.XCHG_KEY_FORMAT).toString());
				if (jobjResult.get(Constants.XCHG_KEY_PAGECOUNT) != null)
					comic.setPageCount(jobjResult.get(Constants.XCHG_KEY_PAGECOUNT).toString());
				if (jobjResult.get(Constants.XCHG_KEY_ISBN) != null)
					comic.setISBN(jobjResult.get(Constants.XCHG_KEY_ISBN).toString());

				JSONObject jobjThumbnail = (JSONObject) jobjResult.get(Constants.XCHG_KEY_THUMBNAIL);
				if (jobjThumbnail != null)
				{
					if (jobjThumbnail.get(Constants.XCHG_KEY_THUMBNAIL_PATH) != null)
						comic.setThumbnailPath(jobjThumbnail.get(Constants.XCHG_KEY_THUMBNAIL_PATH).toString());
					if (jobjThumbnail.get(Constants.XCHG_KEY_THUMBNAIL_EXT) != null)
						comic.setThumbnailExtension(jobjThumbnail.get(Constants.XCHG_KEY_THUMBNAIL_EXT).toString());
				}

				malComics.add(comic);
			}
		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}

	private void showAppropriateView()
	{
		if (mbGridView)
		{
			mlvComics.setVisibility(View.GONE);
			mgvComics.setVisibility(View.VISIBLE);
		}
		else
		{
			mlvComics.setVisibility(View.VISIBLE);
			mgvComics.setVisibility(View.GONE);
		}
	}
}