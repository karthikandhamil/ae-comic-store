/*
 * ImageDownloader
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

class ImageDownloader extends AsyncTask<String, Void, Bitmap>
{
	private ImageView mImage;

	public ImageDownloader(ImageView imageView)
	{
		this.mImage = imageView;
	}

	protected Bitmap doInBackground(String... URLs)
	{
		String strURLs = URLs[0];
		Bitmap bitmap = null;
		try
		{
			InputStream is = new java.net.URL(strURLs).openStream();
			bitmap = BitmapFactory.decodeStream(is);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return bitmap;
	}

	protected void onPostExecute(Bitmap bitmap)
	{
		mImage.setImageBitmap(bitmap);
	}
}