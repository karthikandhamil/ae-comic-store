/*
 * DialogSettings
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class DialogSettings extends Dialog
{
	// Android
	private SharedPreferences mPreferences;
	private SharedPreferences.Editor mPrefEditor;

	// UI
	private ToggleButton mtbShowImages;

	public DialogSettings(Context context, SharedPreferences preferences)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.dialog_settings);

		mPreferences = preferences;
	}

	public void displayDialog()
	{
		intializeViews();
		setViewData();
		addActions();

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		getWindow().setAttributes(lp);
		show();
	}

	private void intializeViews()
	{
		mtbShowImages = (ToggleButton) findViewById(R.id.tb_pref_showimage);

		return;
	}

	private void setViewData()
	{
		boolean bShowImages = mPreferences.getBoolean(Constants.PREF_SHOW_IMAGES, Constants.DEF_SHOW_IMAGES);
		mtbShowImages.setChecked(bShowImages);
	}

	private void addActions()
	{
		mPrefEditor = mPreferences.edit();

		mtbShowImages.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				mPrefEditor.putBoolean(Constants.PREF_SHOW_IMAGES, isChecked);
				mPrefEditor.commit();

				setViewData();
			}
		});
	}
}