/*
 * ActivityDetail
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityDetail extends Activity
{
	// UI Controls
	private ImageView mivComic;
	private TextView mtvTitleKey;
	private TextView mtvTitleValue;
	private TextView mtvDescriptionKey;
	private TextView mtvDescriptionValue;
	private TextView mtvFormatKey;
	private TextView mtvFormatValue;
	private TextView mtvPagecountKey;
	private TextView mtvPagecountValue;
	private TextView mtvISBNKey;
	private TextView mtvISBNValue;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);

		ActionBar currentActionBar = getActionBar();
		currentActionBar.setDisplayShowHomeEnabled(false);

		ColorDrawable colorDrawable = new ColorDrawable();
		colorDrawable.setColor(ContextCompat.getColor(getBaseContext(), R.color.actionbar_bg));
		currentActionBar.setBackgroundDrawable(colorDrawable);

		mivComic = (ImageView) findViewById(R.id.iv_about);
		mtvTitleKey = (TextView) findViewById(R.id.tv_about_title_key);
		mtvTitleValue = (TextView) findViewById(R.id.tv_about_title_value);
		mtvDescriptionKey = (TextView) findViewById(R.id.tv_about_description_key);
		mtvDescriptionValue = (TextView) findViewById(R.id.tv_about_description_value);
		mtvFormatKey = (TextView) findViewById(R.id.tv_about_format_key);
		mtvFormatValue = (TextView) findViewById(R.id.tv_about_format_value);
		mtvPagecountKey = (TextView) findViewById(R.id.tv_about_pagecount_key);
		mtvPagecountValue = (TextView) findViewById(R.id.tv_about_pagecount_value);
		mtvISBNKey = (TextView) findViewById(R.id.tv_about_isbn_key);
		mtvISBNValue = (TextView) findViewById(R.id.tv_about_isbn_value);

		setData(getIntent().getExtras().getString(Constants.XCHG_KEY_TITLE),
				getIntent().getExtras().getString(Constants.XCHG_KEY_DESC),
				getIntent().getExtras().getString(Constants.XCHG_KEY_FORMAT),
				getIntent().getExtras().getString(Constants.XCHG_KEY_PAGECOUNT),
				getIntent().getExtras().getString(Constants.XCHG_KEY_ISBN),
				getIntent().getExtras().getString(Constants.XCHG_KEY_THUMBNAIL_PATH),
				getIntent().getExtras().getString(Constants.XCHG_KEY_THUMBNAIL_EXT));
	}

	private void setData(String strTitle, String strDescription, String strFormat, String strPagecount, String strISBN,
			String strThumbnailPath, String strThumbnailExtension)
	{
		SharedPreferences preferences = getSharedPreferences(Constants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
		boolean bShowImages = preferences.getBoolean(Constants.PREF_SHOW_IMAGES, Constants.DEF_SHOW_IMAGES);

		if (bShowImages)
		{
			mivComic.setVisibility(View.VISIBLE);
			String strURL = strThumbnailPath + "/detail." + strThumbnailExtension;
			ImageDownloader taskDownloadImage = new ImageDownloader(mivComic);
			taskDownloadImage.execute(strURL);
		}
		else
		{
			mivComic.setVisibility(View.GONE);
		}

		if (strTitle.isEmpty())
		{
			mtvTitleKey.setVisibility(View.GONE);
			mtvTitleValue.setVisibility(View.GONE);
		}
		else
		{
			mtvTitleKey.setVisibility(View.VISIBLE);
			mtvTitleValue.setVisibility(View.VISIBLE);
			mtvTitleValue.setText(strTitle);
		}

		if (strDescription.isEmpty())
		{
			mtvDescriptionKey.setVisibility(View.GONE);
			mtvDescriptionValue.setVisibility(View.GONE);
		}
		else
		{
			mtvDescriptionKey.setVisibility(View.VISIBLE);
			mtvDescriptionValue.setVisibility(View.VISIBLE);
			mtvDescriptionValue.setText(strDescription);
		}

		if (strFormat.isEmpty())
		{
			mtvFormatKey.setVisibility(View.GONE);
			mtvFormatValue.setVisibility(View.GONE);
		}
		else
		{
			mtvFormatKey.setVisibility(View.VISIBLE);
			mtvFormatValue.setVisibility(View.VISIBLE);
			mtvFormatValue.setText(strFormat);
		}

		if (strPagecount.isEmpty())
		{
			mtvPagecountKey.setVisibility(View.GONE);
			mtvPagecountValue.setVisibility(View.GONE);
		}
		else
		{
			mtvPagecountKey.setVisibility(View.VISIBLE);
			mtvPagecountValue.setVisibility(View.VISIBLE);
			mtvPagecountValue.setText(strPagecount);
		}

		if (strISBN.isEmpty())
		{
			mtvISBNKey.setVisibility(View.GONE);
			mtvISBNValue.setVisibility(View.GONE);
		}
		else
		{
			mtvISBNKey.setVisibility(View.VISIBLE);
			mtvISBNValue.setVisibility(View.VISIBLE);
			mtvISBNValue.setText(strISBN);
		}
	}

}