/*
 * AdapterComics
 * 
 * @ author: Karthik Palanivelu
 * @ email: palanivelukarthik@gmail.com
 * 
 */

package com.example.aecomics;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterComics extends ArrayAdapter<Comic>
{
	// Primitives
	private boolean mbGridView;
	private int mnLayoutResourceId;
	private ArrayList<Comic> mListData;

	// Android
	private Context mContext;
	private SharedPreferences mPreferences;

	// UI
	private ActivityMain mActivityList;
	private FavouritesListItem mListItemHolder;

	static class FavouritesListItem
	{
		ImageView ivComicCover;
		TextView tvComicTitle;
	}

	public AdapterComics(Context context, int layoutResourceId, ArrayList<Comic> listData, ActivityMain activityMain,
			SharedPreferences preferences, boolean bGridView)
	{
		super(context, layoutResourceId, listData);

		mContext = context;
		mnLayoutResourceId = layoutResourceId;
		mListData = listData;
		mActivityList = activityMain;
		mPreferences = preferences;
		mbGridView = bGridView;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		View row = convertView;
		if (row == null)
		{
			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
			row = inflater.inflate(mnLayoutResourceId, parent, false);
			intializeViews(row);
			row.setTag(mListItemHolder);
		}
		else
		{
			mListItemHolder = (FavouritesListItem) row.getTag();
		}

		if (mbGridView)
		{
			mListItemHolder.ivComicCover.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					displayDetail(position);
				}
			});
		}

		row.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				displayDetail(position);
			}
		});

		setViewData(position);

		return row;
	}

	private void intializeViews(View row)
	{
		mListItemHolder = new FavouritesListItem();
		mListItemHolder.tvComicTitle = (TextView) row.findViewById(R.id.tv_listitem_comic);
		mListItemHolder.ivComicCover = (ImageView) row.findViewById(R.id.iv_listitem_comic);

		return;
	}

	private void setViewData(final int position)
	{
		boolean bShowImages = mPreferences.getBoolean(Constants.PREF_SHOW_IMAGES, Constants.DEF_SHOW_IMAGES);

		Comic comic = mListData.get(position);

		if (!mbGridView)
		{
			mListItemHolder.tvComicTitle.setVisibility(View.VISIBLE);
			String strComicName = comic.getTitle();
			String strFormat = "%01d";
			if (mListData.size() > 0 && mListData.size() < 10)
				strFormat = "%01d";
			else if (mListData.size() > 9 && mListData.size() < 100)
				strFormat = "%02d";
			else if (mListData.size() > 99 && mListData.size() < 1000)
				strFormat = "%03d";
			mListItemHolder.tvComicTitle.setText(String.format(strFormat, (position + 1)) + ". " + strComicName);

		}
		else
		{
			mListItemHolder.tvComicTitle.setVisibility(View.GONE);
		}

		if (bShowImages)
		{
			mListItemHolder.ivComicCover.setVisibility(View.VISIBLE);

			String strURL = comic.getThumbnailPath() + "/standard_xlarge." + comic.getThumbnailExtension();
			ImageDownloader imageDownloader = new ImageDownloader(mListItemHolder.ivComicCover);
			imageDownloader.execute(strURL);
		}
		else
		{
			mListItemHolder.ivComicCover.setVisibility(View.GONE);
		}

		return;
	}

	private void displayDetail(int position)
	{
		Comic comic = mListData.get(position);

		Intent intent = new Intent(mActivityList, ActivityDetail.class);
		intent.putExtra(Constants.XCHG_KEY_TITLE, comic.getTitle());
		intent.putExtra(Constants.XCHG_KEY_DESC, comic.getDescription());
		intent.putExtra(Constants.XCHG_KEY_FORMAT, comic.getFormat());
		intent.putExtra(Constants.XCHG_KEY_PAGECOUNT, comic.getPageCount());
		intent.putExtra(Constants.XCHG_KEY_ISBN, comic.getISBN());
		intent.putExtra(Constants.XCHG_KEY_THUMBNAIL_PATH, comic.getThumbnailPath());
		intent.putExtra(Constants.XCHG_KEY_THUMBNAIL_EXT, comic.getThumbnailExtension());

		mActivityList.startActivity(intent);
	}
}